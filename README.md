
## Description

This is an implemention of the drone task on Nestjs(NodeJs Framework). Th project is structured into modules with the main modules being `drones` and `medication`. The common folder in contains sub folders `decorators` and `helpers` for all custom reusable decorators and helper files.

A Custom response handler is used throughout all controllers to ensure consistency in reponse for any enigneers (FrontEnd, Mobile, AI) who would be consuming the APIS

This Application is also documented with Swagger, please navigate http://localhost:3000/api-doc#/ on your local to view complete documentation.

SQlite was used as the inMemory db choice. We have preloaded a few drones, but feel free to create as many as u wish.

All commits were made on the master, this wasnt intentional.

With another db in view we can have our Drone table with a property that could keep count of how many similar items we have. This means that instead of having a Medication array we could have a nested object with count and medication properties. This might be very useful for anybody integrating.

## Installation
```bash
clone the repository:
$ git clone https://gitlab.com/P-BrainEconnect/musala-task.git
navigate to the project directory

run the installation command 
$ npm install
```

## Running the app
run any of the following commands

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

view on Swagger
http://localhost:3000/api-doc#/
```

## Test
No tests have been written at this point
```bash
# unit tests
# $ npm run test

# e2e tests
# $ npm run test:e2e

# test coverage
# $ npm run test:cov
```
