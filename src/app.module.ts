import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DronesModule } from './drones/drones.module';
import { MedicationModule } from './medication/medication.module';

@Module({
  imports: [
    DronesModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'dronesDB',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true, //safe to say i understand the effect of this and CANNOT be use in prod.
    }),
    MedicationModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
