import { HttpStatus, UnsupportedMediaTypeException } from '@nestjs/common';
import { ResponseError } from './response.dto';

export function fileMimeTypeFilter(mimeTypes: string[]) {
  return (
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error | null, acceptFile: boolean) => void,
  ) => {
    mimeTypes.some((mime) => file.mimetype.includes(mime))
      ? callback(null, true)
      : callback(
          new UnsupportedMediaTypeException(
            new ResponseError(
              `You can only upload ${mimeTypes.join(', ')} files`,
              HttpStatus.UNSUPPORTED_MEDIA_TYPE,
            ),
          ),
          false,
        );
  };
}
