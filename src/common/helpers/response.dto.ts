import { IResponse } from './response.interface';

export class ResponseError implements IResponse {
  constructor(infoMessage: string, status: any, data?: any) {
    this.success = false;
    this.message = infoMessage;
    this.data = data;
    this.status = status;
  }
  message: string;
  data: any[];
  errorMessage: any;
  error: any;
  success: boolean;
  status: any;
  name: any;
}

export class ResponseSuccess implements IResponse {
  constructor(infoMessage: string, status: any, data?: any) {
    this.success = true;
    this.message = infoMessage;
    this.data = data;
    this.status = status;
  }
  message: string;
  data: any;
  errorMessage: any;
  error: any;
  success: boolean;
  status: any;
}
