import { Body, Controller, Get, HttpStatus, Param, Post } from '@nestjs/common';
import { ApiBody, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Ok } from 'src/common/helpers/appResponseType';
import { ResponseSuccess } from 'src/common/helpers/response.dto';
import {
  GetDroneResponse,
  RegisterDroneDTO,
  LoadDroneWithItemsDTO,
  GetMedicationResponse,
  DroneStatusResponse,
  GetBatteryLog,
} from 'src/dto';
import { Drones, Medication, BatteryLogger } from 'src/entities';
import { DronesService } from './drones.service';

@ApiTags('Drones')
@Controller('drones')
export class DronesController {
  constructor(private readonly dronesService: DronesService) {}

  /* This is a get request to get all drones. */
  @Get()
  @ApiOperation({ summary: 'get all drones' })
  @ApiOkResponse({ type: GetDroneResponse, isArray: true })
  async getAllDrones(): Promise<Ok<Drones[]>> {
    const response = await this.dronesService.getDrones();
    return new ResponseSuccess('Drones', HttpStatus.OK, response);
  }

  /* This is a post request to register a drone. */
  @Post()
  @ApiOperation({ summary: 'Register a drone' })
  @ApiBody({ type: RegisterDroneDTO })
  @ApiOkResponse({ type: GetDroneResponse })
  async registerDrone(
    @Body() registerDroneDTO: RegisterDroneDTO,
  ): Promise<Ok<Drones>> {
    const response = await this.dronesService.registerDrone(registerDroneDTO);
    return new ResponseSuccess('Drone Added', HttpStatus.OK, response);
  }

  /* This is a post request to load a drone with medication items. */
  @Post(':droneId/load-item')
  @ApiOperation({ summary: 'Load a drone with medication items' })
  @ApiBody({ type: LoadDroneWithItemsDTO })
  @ApiOkResponse({ type: GetDroneResponse })
  async loadDroneWithItems(
    @Body() { items }: LoadDroneWithItemsDTO,
    @Param('droneId') droneId: string,
  ): Promise<Ok<Drones>> {
    const response = await this.dronesService.loadDroneWithItems(
      droneId,
      items,
    );
    return new ResponseSuccess('Medication Added', HttpStatus.OK, response);
  }

  /* This is a get request to check loaded medication items for a given drone. */
  @Get(':droneId/:itemId')
  @ApiOperation({ summary: 'Check loaded medication items for a given drone' })
  @ApiOkResponse({ type: GetMedicationResponse })
  async checkDroneForItems(
    @Param('droneId') droneId: string,
    @Param('itemId') itemId: string,
  ): Promise<Ok<Medication>> {
    const response = await this.dronesService.checkDroneForItems(
      droneId,
      itemId,
    );
    return new ResponseSuccess('Medication', HttpStatus.OK, response);
  }

  /* This is a get request to check availability of drone. */
  @Get(':droneId/status')
  @ApiOperation({ summary: 'check availability of drone' })
  @ApiOkResponse({ type: DroneStatusResponse })
  async checkAvalabilityOfDrones(
    @Param('droneId') droneId: string,
  ): Promise<Ok<DroneStatusResponse>> {
    const response = await this.dronesService.checkAvalabilityOfDrones(droneId);
    return new ResponseSuccess('Drone Status', HttpStatus.OK, response);
  }

  /* This is a get request to get all loggers. */
  @Get('logs')
  @ApiOperation({ summary: 'get all battery log entries' })
  @ApiOkResponse({ type: GetBatteryLog, isArray: true })
  async getAllLoggers(): Promise<Ok<BatteryLogger[]>> {
    const response = await this.dronesService.getLoggers();
    return new ResponseSuccess('Loggers', HttpStatus.OK, response);
  }
}
