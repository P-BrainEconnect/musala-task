import { Module } from '@nestjs/common';
import { DronesService } from './drones.service';
import { DronesController } from './drones.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BatteryLogger, Drones } from '../entities/drones.entity';
import { Medication } from 'src/entities/medication.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Drones, Medication, BatteryLogger])],
  controllers: [DronesController],
  providers: [DronesService],
})
export class DronesModule {}
