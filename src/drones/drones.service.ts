import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseError } from 'src/common/helpers/response.dto';
import { RegisterDroneDTO, DroneStatusResponse } from 'src/dto';
import {
  Drones,
  Medication,
  BatteryLogger,
  DroneState,
  LogLevel,
} from 'src/entities';
import { Repository } from 'typeorm';

@Injectable()
export class DronesService {
  private minimumBatteryCapacity: number;
  constructor(
    @InjectRepository(Drones)
    private dronesRepository: Repository<Drones>,
    @InjectRepository(Medication)
    private medicationRepository: Repository<Medication>,
    @InjectRepository(BatteryLogger)
    private batteryLoggerRepository: Repository<BatteryLogger>,
  ) {
    this.minimumBatteryCapacity = 25;
  }

  async getDrones(): Promise<Drones[]> {
    return await this.dronesRepository.find();
  }

  async registerDrone(registerDroneDTO: RegisterDroneDTO): Promise<Drones> {
    const drone = this.dronesRepository.create({
      ...registerDroneDTO,
      availableSpace: registerDroneDTO.weightLimit,
      state: DroneState.idle,
    });
    return this.dronesRepository.save(drone);
  }
  /**
   * It takes in a drone id and an array of Medication id, it then loops through the items id, confirms if the
   * ids exist, checks if there is enough space left then adds the item to the drone
   * @param {string} droneId - The id of the drone to be loaded
   * @param payload - Array<string> of Medication id to be added to the drone
   * @returns The drone with the items added to it
   */
  async loadDroneWithItems(
    droneId: string,
    payload: Array<string>,
  ): Promise<Drones> {
    let drone = await this.getDrone(droneId);
    // confirm if the drone is in idle or loading state and
    // battery level is greater than the mininmum
    const droneState =
      drone.state == DroneState.idle || drone.state == DroneState.loading;
    const batteryLevel = drone.batteryCapacity > this.minimumBatteryCapacity;
    if (droneState && batteryLevel) {
      drone = await this.addMedicationsToDrone(payload, drone);
      await this.dronesRepository.save(drone);
    }
    return drone;
  }

  /**
   * "Check if the drone has a given item."
   *
   * The function is async because it's going to be making a database call
   * @param {string} droneId - The id of the drone that we want to check for the item.
   * @param {string} itemId - The id of the item that we want to check if the drone has.
   * @returns the item if found or null otherwise
   */
  async checkDroneForItems(
    droneId: string,
    itemId: string,
  ): Promise<Medication> {
    const drone = await this.getDrone(droneId);
    return (
      drone.medications?.find((medication) => medication.id == itemId) || null
    );
  }

  /**
   * It checks if the drone is available to be used or not
   * @param {string} droneId - The id of the drone that we want to check the status of.
   * @returns DroneStatusResponse
   */
  async checkAvalabilityOfDrones(
    droneId: string,
  ): Promise<DroneStatusResponse> {
    const drone = await this.getDrone(droneId);
    return {
      availableSpace: drone.availableSpace,
      state: drone.state,
      batteryLevel: drone.batteryCapacity,
    };
  }

  /* A cron job that runs every 10 seconds to check the battery level of the drones and log it. */
  @Cron(CronExpression.EVERY_10_SECONDS)
  async periodicDroneBatteryCheck() {
    const drones = await this.dronesRepository.find();
    const batteryLogs = drones.map((drone) =>
      this.createBatteryLogInstance(drone),
    );
    await this.batteryLoggerRepository.save(batteryLogs);
  }

  /**
   * It takes an array of medication ids and a drone, finds the medications in the database, adds them to
   * the drone, and returns the drone
   * @param payload - Array<string> - this is the array of medication ids that will be added to the drone
   * @param {Drones} drone - Drones - the drone that we want to add the medication to
   * @returns The drone with the medications added to it.
   */
  async addMedicationsToDrone(
    payload: Array<string>,
    drone: Drones,
  ): Promise<Drones> {
    for (const id of payload) {
      const medication = await this.medicationRepository.findOne({
        where: { id },
      });
      if (!medication) {
        throw new NotFoundException(
          new ResponseError('Invalid medication id', HttpStatus.NOT_FOUND),
        );
      }
      // this solution dosent check if a medication is already added to the drone
      // on the assumption that an item can be added multiple times
      if (drone.availableSpace < medication.weight) {
        throw new BadRequestException(
          new ResponseError('Weight Limit Exceded', HttpStatus.BAD_REQUEST),
        );
      }
      drone.medications
        ? drone.medications.push(medication)
        : (drone.medications = [medication]);
      drone.availableSpace -= medication.weight;
      drone.state == DroneState.idle ?? DroneState.loading;
    }
    return drone;
  }

  /**
   * It creates a new instance of the BatteryLogger class, and then assigns the level property of the new
   * instance to either low or warn, depending on the value of the batteryCapacity parameter
   * @param {Drones}  - Drones - this is the type of the object that is passed in as a parameter.
   * @returns A new instance of BatteryLogger
   */
  createBatteryLogInstance({
    serialNumber,
    batteryCapacity,
  }: Drones): BatteryLogger {
    const newLoggerInstance = this.batteryLoggerRepository.create({
      serialNumber,
      batteryCapacity,
      date: Date.now().toString(),
    });
    if (batteryCapacity < 35) {
      batteryCapacity <= 25
        ? (newLoggerInstance.level = LogLevel.low)
        : (newLoggerInstance.level = LogLevel.warn);
    }
    // console.log(newLoggerInstance);
    return newLoggerInstance;
  }

  async getLoggers(): Promise<BatteryLogger[]> {
    return await this.batteryLoggerRepository.find();
  }

  /**
   * "Get a drone by its id."
   *
   * The first line of the function is a TypeScript annotation. It tells the compiler that the function
   * returns a Promise of a Drones object
   * @param {string} id - The id of the drone we want to retrieve.
   * @returns A drone object or throws an error if id is invalid
   */
  async getDrone(id: string): Promise<Drones> {
    //this is a simple implementation of custom repository classes
    const drone = await this.dronesRepository.findOne({
      where: { id },
    });
    if (!drone) {
      throw new NotFoundException(
        new ResponseError('Invalid drone id', HttpStatus.NOT_FOUND),
      );
    }
    return drone;
  }
}
