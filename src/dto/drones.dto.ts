import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  ArrayUnique,
  IsArray,
  IsOptional,
  Max,
  MaxLength,
  Min,
} from 'class-validator';
import { DroneModel, DroneState, LogLevel } from '../entities/drones.entity';
import { GetMedicationResponse } from './medication.dto';

export class RegisterDroneDTO {
  @MaxLength(100, { message: 'Serial Number must be less than 100 characters' })
  @ApiProperty({ type: String, default: 'SN1' })
  serialNumber: string;

  @ApiProperty({ enum: DroneModel, default: DroneModel.heavyweight })
  model: DroneModel;

  @Max(500, { message: 'Weight limit cannot be more 500gr' })
  @Min(0, { message: 'Weight limit limit cannot be less than 0gr' })
  @ApiProperty({ type: Number, default: 500 })
  weightLimit: number;

  @Max(100, { message: 'Battery Capacity limit cannot be more than 100%' })
  @Min(0, { message: 'Battery Capacity limit cannot be less than 0%' })
  @ApiProperty({ type: Number, default: 100 })
  batteryCapacity: number;
}

export class LoadDroneWithItemsDTO {
  @IsArray()
  @ArrayUnique()
  @IsOptional()
  @ApiPropertyOptional({ type: [String] })
  items?: string[];
}

export class DroneStatusResponse {
  @ApiProperty({ enum: DroneState })
  state: DroneState;

  @ApiProperty()
  availableSpace: number;

  @ApiProperty()
  batteryLevel: number;
}

export class GetDroneResponse {
  @ApiProperty({
    type: String,
    default: '1ab28fde-f5e7-410b-b1e4-b83488f3bfeb',
  })
  id: string;

  @ApiProperty({ type: String, default: 'r834hr87h7r3' })
  serialNumber: string;

  @ApiProperty({ enum: DroneModel, default: DroneModel.heavyweight })
  model: DroneModel;

  @ApiProperty({ type: Number, default: 500 })
  weightLimit: number;

  @ApiProperty({ type: Number, default: 67 })
  batteryCapacity: number;

  @ApiProperty({ enum: DroneState, default: DroneState.idle })
  state: DroneState;

  @ApiProperty({ type: Number, default: 197 })
  availableSpace: number;

  @ApiProperty({ type: [GetMedicationResponse] })
  medications: GetMedicationResponse[];
}

export class GetBatteryLog {
  @ApiProperty({
    type: String,
    default: '895b6423-2230-4b10-aa0a-b7f9428169e6',
  })
  id: string;

  @ApiProperty({ type: String, default: 'r834hr87h7r3' })
  serialNumber: string;

  @ApiProperty({ type: Number, default: 34 })
  batteryCapacity: number;

  @ApiProperty({ enum: LogLevel, default: LogLevel.low })
  level: LogLevel;

  // we're using string data type bcos sqlite dosent support a date field
  @ApiProperty({ type: String, default: '1670844680007' })
  date: string;
}
