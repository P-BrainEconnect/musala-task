import { ApiProperty } from '@nestjs/swagger';
import { Matches } from 'class-validator';
import { Express } from 'express';

export class MedicationDTO {
  @ApiProperty({ type: String })
  @Matches(/^[0-9A-Za-z_-]+$/, {
    message: 'Name field should contain only letters, numbers, ‘-‘, ‘_’',
  })
  name: string;

  @ApiProperty({ type: Number })
  weight: number;

  @ApiProperty({ type: String })
  @Matches(/^[0-9A-Z_]+$/, {
    message:
      'code field should contain only upper case letters, underscore and numbers',
  })
  code: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: true,
  })
  file: Express.Multer.File;
}

export class GetMedicationResponse {
  @ApiProperty({
    type: String,
    default: 'abf9a820-9e6d-4895-93b3-2667183c3116',
  })
  id: string;

  @ApiProperty({ type: String, default: 'Panadol Extra' })
  name: string;

  @ApiProperty({ type: Number, default: 12 })
  weight: number;

  @ApiProperty({ type: String, default: 'UDET_56' })
  code: string;

  @ApiProperty({ type: String, default: '269692690_n.jpeg' })
  image: string;
}
