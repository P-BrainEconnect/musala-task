import { Medication } from 'src/entities/medication.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

export enum DroneModel {
  'lightweight' = 'Lightweight',
  'middleweight' = 'Middleweight',
  'cruiserweight' = 'Cruiserweight',
  'heavyweight' = 'Heavyweight',
}
export enum DroneState {
  'idle' = 'IDLE',
  'loading' = 'LOADING',
  'loaded' = 'LOADED',
  'delivering' = 'DELIVERING',
  'delivered' = 'DELIVERED',
  'returning' = 'RETURNING',
}

export enum LogLevel {
  'okay' = 'OKAY',
  'warn' = 'WARNING',
  'low' = 'LOW',
}
@Entity()
export class Drones {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  serialNumber: string;

  @Column({ enum: DroneModel })
  model: DroneModel;

  @Column()
  weightLimit: number;

  @Column()
  batteryCapacity: number;

  @Column({ enum: DroneState, default: DroneState.idle })
  state: DroneState;

  @Column()
  availableSpace: number;

  @OneToMany(() => Medication, (medication) => medication.drone, {
    eager: true,
  })
  medications: Medication[];
}

@Entity()
export class BatteryLogger {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  serialNumber: string;

  @Column()
  batteryCapacity: number;

  @Column({ enum: LogLevel, default: LogLevel.okay })
  level: LogLevel;

  @Column()
  date: string;
}
