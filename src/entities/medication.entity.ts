import { Drones } from 'src/entities/drones.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class Medication {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  weight: number;

  @Column()
  code: string;

  @ManyToOne(() => Drones, (drone) => drone.medications)
  drone: Drones;

  @Column()
  image: string;
}
