import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  UploadedFile,
} from '@nestjs/common';
import { ApiOperation, ApiBody, ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { GetMedicationResponse, MedicationDTO } from 'src/dto';
import { Medication } from 'src/entities';
import { MedicationService } from './medication.service';
import { Ok } from 'src/common/helpers/appResponseType';
import { ResponseSuccess } from 'src/common/helpers/response.dto';
import { ApiFile } from 'src/common/decorators/UploadField';
import { fileMimeTypeFilter } from 'src/common/helpers/fileTypeFilter';

@ApiTags('Medication')
@Controller('medication')
export class MedicationController {
  constructor(private readonly medicationService: MedicationService) {}

  /* This is a get request to get all the medications. */
  @Get()
  @ApiOperation({ summary: 'Get all medications' })
  @ApiOkResponse({ type: GetMedicationResponse, isArray: true })
  async getAllDrones(): Promise<Ok<Medication[]>> {
    const response = await this.medicationService.getMedication();
    return new ResponseSuccess('Medications', HttpStatus.OK, response);
  }

  /* This is a post request to create a new medication. */
  @Post()
  @ApiOperation({ summary: 'Create medication' })
  @ApiBody({ type: MedicationDTO })
  @ApiFile('file', {
    dest: './uploads',
    fileFilter: fileMimeTypeFilter(['image']),
  })
  @ApiOkResponse({ type: GetMedicationResponse })
  async uploadMedication(
    @Body() medicationDTO: MedicationDTO,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Ok<Medication>> {
    const response = await this.medicationService.uploadMedication(
      medicationDTO,
      file,
    );
    return new ResponseSuccess('Medication Created', HttpStatus.OK, response);
  }
}
