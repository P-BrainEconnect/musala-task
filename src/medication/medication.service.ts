import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MedicationDTO } from 'src/dto';
import { Medication } from 'src/entities';
import { Repository } from 'typeorm';

@Injectable()
export class MedicationService {
  constructor(
    @InjectRepository(Medication)
    private medicationRepository: Repository<Medication>,
  ) {}

  /**
   * It returns a list of all medications in the database
   * @returns An array of Medication objects
   */
  async getMedication(): Promise<Medication[]> {
    return await this.medicationRepository.find();
  }

  /**
   * It creates a new medication object, saves it to the database, and returns it
   * @param {MedicationDTO} medicationDTO - MedicationDTO - The properties of the medication object.
   * @param file - Express.Multer.File
   * @returns The medication object is being returned.
   */
  async uploadMedication(
    medicationDTO: MedicationDTO,
    file: Express.Multer.File,
  ): Promise<Medication> {
    const medication = this.medicationRepository.create({
      ...medicationDTO,
      image: file.originalname,
    });
    return this.medicationRepository.save(medication);
  }
}
